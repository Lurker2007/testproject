package ru.home;


import ru.home.oop.Box;

public class ClassWork3 {
  // Абстракция
  // Инкапсуляция
  // Наследование
  // Полиморфизм (?)

  static {
    //Статический
  }

  public static void main(String[] args) {
    Box myBox = new Box(10, 14, 12);
    Box myBox2 = new Box(11, 15, 13);
    Box myBox3 = new Box(12, 16, 13);
    Box myBox4 = new Box(13, 17, 15);
    Box myBox5 = new Box(14, 18, 16);

    myBox.getLenght();
    System.gc();

    int length = 10;
    int width = 10;

    Box box = new Box(1,2,3);
    Box box2 = box;

    box.getSomeValue(1);
    //box2.lenght = 5;

    System.out.println(box.toString());


    System.out.println(myBox);
    System.out.println(myBox5);

    myBox.addToBox(length, width);
    myBox.swapBox(myBox, myBox5);

    System.out.println(myBox);
    System.out.println(myBox5);

    myBox.changeBox(myBox);

    System.out.println(myBox);

    System.out.println("Legnth " + length);
    System.out.println("Width " + width);



    //Haski myDog = new Haski();
    //Dog myDog = new Haski();
    //Animal myDog = new Haski();

    // private - только внутри класса
    // default (package) - только пакет
    // protected - только родитель или пакет
    // public - виден отовсюду

  }
}
