package ru.home.oop;

public class Box {

  private int lenght;
  private int width;
  private int height;
  private String material;
//  public int weight;

  {
    System.out.println("Создается объект");
    //нестатический блок
  }

  public void setLenght(int lenght) {
    this.lenght = lenght;
  }

  public int getLenght() {
    return lenght;
  }

  public Box(int lenght, int width, int height) { //int weight) {
    this.lenght = lenght;
    this.width = width;
    this.height = height;
//    this.weight = weight;
  }

  @Override
  public void finalize() throws Throwable {
    //scanner.close();
  }

//  public Box(int lenght, int width, int height) {
//    this(lenght, width, height, 0);
//  }

  // type methodName(args){
  //  ...
  //  return typeVar
  // }

  // void methodName(args){
  //  ...
  // }

  public int getVolume() {
    return lenght * width * height;
  }

  public void addToBox(int lenght, int width, int height) {
    this.lenght -= lenght;
    this.width -= width;
    this.height -= height;
  }

//  public void addToBox(int lenght, int width) {
//    this.lenght -= lenght;
//    this.width -= width;
//  }

  public void addToBox(int lenght, double width) {
//    this.lenght -= lenght;
//    this.width -= (int) width;
    lenght *= 2;
    width /= 2;
  }

  public void swapBox(Box box1, Box box2) {
    Box tmp = box1;
    box1 = box2;
    box2 = tmp;
    // Логика
  }

  public void changeBox(Box b) {
    b.lenght *= 2;
    b.width /= 2;
  }

  @Override
  public String toString() {
    return "Length " + lenght + " Width " + width + " Height " + height;
  }

  //  public Box addToBox(int lenght, double width) {
//    this.lenght -= lenght;
//    this.width -= (int) width;
//    return this;
//  }
  public int getLengthFact(int length) {
    int result;
    if (length == 1) {
      return 1;
    }
    return getLengthFact(length - 1) * length;
  }


  public int getSomeValue(int... args) {
    int[] args1 = args;
    return 1;
  }

  public int getSomeValue(String i, int... args) {
    int[] args1 = args;
    return 1;
  }
}

// class ClassName {
// [class variables]
// ClassName(arg) - constructors
// type method
// }
