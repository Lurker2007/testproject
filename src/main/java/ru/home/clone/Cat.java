package ru.home.clone;

import ru.home.anotation.LifeDetector;

public class Cat implements Cloneable{
  @LifeDetector
  int age;
  Type type;

  public Cat(int age, Type type) {
    this.age = age;
    this.type = type;
  }

  public void makeNoise() {
    System.out.println("Мяу");
  }
  public String getName() {
    return "name";
  }

  @Override
  public Cat clone() throws CloneNotSupportedException {
    return (Cat) super.clone();
  }

  @Override
  public String toString() {
    return (type + " " + String.valueOf(age));
  }

  // Цикл от 1 до 100
  // Если число делится на 3 то выводим fizz
  // Если число деится на 5 то выводим buzz
  // Если число делится на 15 то выводим fiizbuzz
  // Во всех остальных случаях просто выводим число
}
