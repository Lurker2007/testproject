package ru.home.clone;

import java.util.Scanner;

public class MainClone {
  public static void main(String[] args) throws CloneNotSupportedException {
    Type type = new Type("Длиннлшерстная");
    Cat cat = new Cat(2, type);
    Cat cloneCat = cat.clone();
    System.out.println(cat);
    System.out.println(cloneCat);
    cat.age = 5;
    cloneCat.age = 1;
    System.out.println(cat);
    System.out.println(cloneCat);
    cat.type.feature = "QQQQ";
    System.out.println(cloneCat);
    cloneCat.type.feature = "WWWWW";
    System.out.println(cat);
    System.out.println(cloneCat);

  }
}
