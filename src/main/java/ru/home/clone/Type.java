package ru.home.clone;

public class Type {
  String feature;

  public Type(String feature) {
    this.feature = feature;
  }

  @Override
  public String toString() {
    return feature;
  }
}
