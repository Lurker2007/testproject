package ru.home;

import java.util.Scanner;

public class Main {

  public static final int HEX;

  static {
    HEX = 0xA1;
  }

  public static void main(String[] args) {
    int number = 1;

    System.out.println("Здадание 1");
    {


    }

    System.out.println("-------");
    System.out.println("Задание 2");
    /*
    1. Целочисленные
    byte 8 бит (1 байт) -2^7 .... 2^7 - 1    0_000_0000 ----- 0000_0001 -> 11111110 + 1 -> 1111_1111
    short 16 бит (2 байта) -2^15 .... 2^15 - 1
    int 32 бит (4 байта) -2^31 ... 2^31 - 1
    long 64 бит (8 байт) -2^63 ... 2^63 - 1
    */

    /*
    2. С плавающей точкой
    float 32 бит
    double 64 бит
    */

    /*
    3. Символы
    char 16 бит 0 .... 2^16
    */
//default = 0
/*
    3. boolean
    true / false
    */
//default = false

// int -> String -> 3 String -> 3 int;

    System.out.println("Byte");
    System.out.println("Max " + Byte.MAX_VALUE + ", Min: " + Byte.MIN_VALUE);
    System.out.println("Short");
    System.out.println("Max " + Short.MAX_VALUE + ", Min: " + Short.MIN_VALUE);
    System.out.println("Integer");
    System.out.println("Max " + Integer.MAX_VALUE + ", Min: " + Integer.MIN_VALUE);
    System.out.println("Long");
    System.out.println("Max " + Long.MAX_VALUE + ", Min: " + Long.MIN_VALUE);


    int hex = HEX;

    System.out.println(hex);

    System.out.println("Double");
    System.out.println("Max " + Double.valueOf(1.213));
    System.out.println("Float");
    System.out.println("Max " + Float.valueOf(1.123f));

    int x = 10;
    if (x == 10) {
      //int x = 40
      x = x + 5;
      int y = 20;
    }
    System.out.println(x);

    System.out.println(Gender.MALE.toString());
    System.out.println(Gender.FEMALE.toString());
//    System.out.println(y);


    byte b1 = 1;
    short s1 = 2;
    int i1 = 3;
    long l1 = 4L;
    float f1 = 5f;
    double d1 = 6.0;
    char c1 = 7;

    long i = b1 + s1 + i1 + 100 + l1;
    float v = i1 + f1;
    double v1 = l1 + f1;
    double v2 = d1 + l1;
    double v3 = d1 + i1;

    double i2 = 5.0 / 2;


    i1 = (int) l1;

    short max = Byte.MAX_VALUE + 1; //0000_0000_1000_0000
    byte b = (byte) max; // 1111_1111 -> - !(111_1110) -> - (000_0001) = -1 // 1000_0000 - 1 = - !(0111_1111) = - 10000000 = -128
    System.out.println(b);


    int division = 15 % 7;

    int i4 = 1;
    i4 = i4 + 2;
    i4 += 2;

    i4 = 10;

    System.out.println(i4++);
    System.out.println(i4--);

    System.out.println(++i4);
    System.out.println(--i4);

    //if (4 == 2)
    //if (4 != 2)
    //if (4 > 2)
    //if (4 < 2)
    //if (4 <= 2)
    //if (4 >= 2)
    //if (!(4 > 2) && (4 < 2))
    //if (!(4 > 2) || (4 < 2))

    i4 = (4 > 2) ? 4 : 2; // (exp ? var1 : var 2), если exp = true => var1, else var2
    i4 = i4 >> 2;
    System.out.println(division);

    Math.pow(i4, i4);
    Math.sqrt(i4);

    // if (exp) {
    // body
    // } if else {
    //  body
    // }
    // else {
    // boyd2
    // }


    int i5 = 1;
    int borderValue = 5;

    if (i5 < borderValue) {
      System.out.println(i5 + " Less then " + borderValue);
    } else if (i5 == borderValue) {
      System.out.println(i5 + " Equals " + borderValue);
    } else {
      System.out.println(i5 + " More than " + borderValue);
    }

    // while (condition) {
    //  body
    // }

    int i6 = 11;
    while (i6 <= 10) {
      System.out.println(i6);
      //i6++;
      i6 = i6 + 1;
    }

//    while(true) {
//      if(i6 >=10) {
//        break;
//      }
//    }

    System.out.println("----------------------");
    int i7 = 11;

    do {
      System.out.println(i7++);
    } while (i7 <= 10);

    //for(init; condition; step) {
    //  body
    // }
    System.out.println("----------------------");

    for (int j = 0; j <= 10; j++) {
      System.out.println(j);
    }
//    for(;;){
//
//    }

    System.out.println("----------------------");
    for (int j = 0; j < 10; j++) {
      System.out.println(j);
      if (j == 3) {
        System.out.println("Stop");
        break;
      }
    }
    System.out.println("End cicle");
    System.out.println("----------------------");
    there:

      for (int j = 0; j < 10; j++) {
        for (int jj = 0; jj < 10; jj++) {
          System.out.println("j = " + j + ", jj = " + jj);
          if (jj > 3 && jj < 6) {
            break there;
          }
        }
      }
      ////
      ///
      ////
      System.out.println("+++++++");

    ////////

    System.out.println("----------------------");

    int i8 = 3;
    switch (i8) {
      case 3:
        System.out.println("Три");
      case 1:
        System.out.println("Один");
      case 2:
        System.out.println("Два");
        break;
      default:
        System.out.println("Число");
        break;
    }

    System.out.println("----------------------");
    Gender gender = Gender.SHEMALE;

    switch (gender) {
      case MALE:
        System.out.println("Мужчина");
        break;
      case FEMALE:
        System.out.println("Женщина");
        break;
      default:
          System.out.println("ОНО");
    }


    // 131 % 10 = 1 --
    // 131 / 10 = 13
    // 13 % 10 = 3 --
    // 13 / 10 = 1 --

    //ClassWork2.print();
  }
}
