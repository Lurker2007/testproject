package ru.home.stream;

import java.io.Serializable;

public class Cat implements Serializable {

    static final long serialVersionUID = 2L;

    private String name;
    private int age;
    private String type;
    private String type2;

    public Cat(String name, int age, String type) {
        this.name = name;
        this.age = age;
        this.type = type;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", type='" + type + '\'' +
                '}';
    }
}
