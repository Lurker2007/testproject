package ru.home.stream;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.Externalizable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Reader;
import java.io.Writer;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //byte
        InputStream inputStream; //read
        OutputStream outputStream1; //write

        //char
        Reader reader1;
        Writer writer;

        PrintStream out = System.out;
        InputStream in = System.in;
        PrintStream err = System.err;

        FileInputStream fileInputStream;
        FileOutputStream fileOutputStream;
//        Scanner scanner = new Scanner(System.in);
//        String s = scanner.nextLine();
//        scanner.close();

//        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
//            int read = inputStreamReader.read();
//            System.out.println(read);
//            char read1 = (char) read;
//            System.out.println(String.valueOf(read1));
//            String s = reader.readLine();
//            System.out.println(s);
//        } catch (IOException e) {
//
//        }

        File file = new File("1.txt");
        String pathname = "\\Projects\\testproject";
        File file2 = new File(pathname);
        if (file2.isDirectory()) {
            String[] list = file2.list();
            for (String str : list) {
                File file3 = new File(pathname + "\\" + str);
                if(file3.isDirectory()) {
                    System.out.println(str);
                }
            }
        }

        try (FileInputStream inputStreamFile = new FileInputStream(file)) {
            int read;
            StringBuilder result = new StringBuilder();
            do {
                read = inputStreamFile.read();
                result.append((char) read);
            } while (read != -1);
            System.out.println(result);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        try (FileOutputStream outputStream = new FileOutputStream("2.txt", false)) {
            String s = "qwrqyuietqweuiqwie";
            outputStream.write(s.getBytes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ByteArrayInputStream byteArrayInputStream;

        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("cat.txt"))) {
            Cat vasya = new Cat("Vasya", 3, "Cheshir");
            oos.writeObject(vasya);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("cat.txt"))) {
            Object o = ois.readObject();
            if (o instanceof Cat) {
                Cat o1 = (Cat) o;
                System.out.println(o1);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        //Externalizable;

        try(BufferedReader br = new BufferedReader(new FileReader(file))) {
            String s;
            int i = 0;
            while ((s = br.readLine())!= null) {
                i++;
                if(i % 2 == 0) {
                    System.out.println(s);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        try(BufferedWriter br = new BufferedWriter(new FileWriter("3.txt"))) {
            br.write("qeqweqweqweqwe");
            br.newLine();
            br.write(";gljk;ghljk");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }



    }

}
