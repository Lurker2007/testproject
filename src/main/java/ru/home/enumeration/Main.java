package ru.home.enumeration;

public class Main {

    public static void main(String[] args) {
        int code = City.Kirov.getCode();
        System.out.println(code);
        System.out.println(code);
        City myCity = City.Kirov;
        City moscow = City.valueOf("Moscow");
        City[] values = City.values();
        System.out.println();
        String s = "1";
        String s2 = "2";
        if (myCity == City.Moscow) {
            System.out.println("myCity = Moscow");
        } else {
            System.out.println("MyCity is not Moscow");
        }

        switch (myCity) {
            case Kirov:
                System.out.println("MyCity is not Moscow");
                break;
            default:
                System.out.println("Unknown city");
        }
    }

}
