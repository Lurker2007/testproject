package ru.home.enumeration;

//public class City extends Enum
public enum City {
    Moscow(905, 4.15),
    Rostov(194, 1.98),
    Krasnodar(491, 2.69),
    Kirov(800, 5);

    private int code;
    private double price;

    private City(int code, double price) {
        this.code = code;
        this.price = price;
    }

    public int getCode() {
        return code;
    }

    public double getPrice() {
        return price;
    }
}
