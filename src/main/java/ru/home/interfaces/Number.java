package ru.home.interfaces;

public interface Number extends OldNumber, QNumber {

  void add(int number);
  void substraction(int number);

  default void getNumber() {
    System.out.println("1");
  }

  default void anotherMethod() {
    System.out.println("qqq");
  }
}
