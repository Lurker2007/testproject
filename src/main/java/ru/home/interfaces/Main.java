package ru.home.interfaces;

public class Main {
  public static void main(String[] args) {
    Number smallNumber = new SmallNumber();
    Number bigNumber = new BigNumber();

    smallNumber.add(1);
    bigNumber.add(1);

    smallNumber.print();

    QNumber q = new SmallNumber();
    q.print();
  }
}
