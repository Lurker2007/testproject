package ru.home.exception;

import java.io.File;
import java.io.InputStream;

public class Main {
  public static void main(String[] args) {
    int[] array = new int[2];
//    int q = array[5];
//    System.out.println(q);
//    String a = null;
//    System.out.println(a.equals(null));

    try {
      print(true);
    } catch (CheckedException e) {
      e.printStackTrace();
      System.out.println("Ошибочка вышла");
    } catch (AnotherCheckedException e) {
      System.out.println("Другая ошибочка вышла");
    } finally {
      System.out.println("Finally");
    }

    System.out.println("qqqq");

    System.out.println(getNumber(true));
    System.out.println(getNumber(false));
  }

  private static void print(boolean q) throws CheckedException, AnotherCheckedException {
    if (q) {
      throw new AnotherCheckedException();
    } else {
      throw new CheckedException("MethodNotSupport");
    }
  }

  private static void print2() throws UncheckedException {
    throw new UncheckedException("MethodNotSupport");
  }

  private static int getOne(boolean q) throws CheckedException {
    if (q) {
      throw new CheckedException("Нельзя");
    }
    return 1;
  }

  private static int getNumber(boolean flag) {
    try {
      getOne(flag);

    } catch (CheckedException e) {
      e.printStackTrace();
      System.out.println(e);
      return 2;
    } finally {
      return 3;
    }
  }

}
