package ru.home.exception;

public class CheckedException extends Exception {

  public CheckedException(String message) {
    super(message);
  }
}
