package ru.home.anotation;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import ru.home.clone.Cat;
import ru.home.clone.Type;

public class Main {
    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException {
        Cat cat = new Cat(50, new Type("Кошка"));
        cat.getName();
        Field[] declaredFields = Cat.class.getDeclaredFields();
//        Method[] methods = Cat.class.getMethods();
//        for (Method method : methods) {
//            Class<?> returnType = method.getReturnType();
//            System.out.println(returnType.cast(method.invoke(cat)));
//        }

        for(Field field : declaredFields) {
            if(field.isAnnotationPresent(LifeDetector.class)){
                Class<?> type = field.getType();
                field.setAccessible(true);
                field.set(cat, 1000);
                Object o = field.get(cat);
                Integer catAge = (Integer) o;
                LifeDetector annotation = field.getAnnotation(LifeDetector.class);
                if(catAge > annotation.value()) {
                    System.out.println("Нереальный возраст для живности, она уже умерла");
                } else {
                    System.out.println("Все ок");
                }
            }
        }
    }
}
