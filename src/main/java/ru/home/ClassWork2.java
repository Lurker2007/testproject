package ru.home;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Scanner;

public class ClassWork2 {

  public static void main(String[] args) {

    //Type[] arrayName = new Type[size]
    int[] array = new int[10];
    int[] array1 = {1, 2, 3, 4, 5};
    //Type name = new Type(args..);

    System.out.println(array1[2]);

    //int i = 10;

    for (int i = 0; i < array.length; i++) {
      array[i] = (int) (Math.random() * 100);
    }

    for (int i = 0; i < array.length; i++) {
      //System.out.println(array[i]);
    }

    for (int i : array) { //foreach
      System.out.println(i);
    }

    Arrays.sort(array);
    System.out.println("----------------------");
    String x = Arrays.toString(array);
    System.out.println(x);

    int[] array3 = Arrays.copyOf(array, 15);
    System.out.println(Arrays.toString(array3));
    System.out.println(array3.length);

    int[] newArray = new int[10];
    int[] newArray2 = new int[10];

    System.out.println(newArray == newArray2);
    System.out.println(newArray.equals(newArray2));
    System.out.println(Arrays.equals(newArray, newArray2));

    int[][] multiArray = new int[3][3];
    for (int i = 0; i < multiArray.length; i++) {
      for (int j = 0; j < multiArray[0].length; j++) {
        multiArray[i][j] = i + j;
      }
    }

    for (int i = 0; i < multiArray.length; i++) {
      for (int j = 0; j < multiArray[0].length; j++) {
        System.out.print(multiArray[i][j] + " ");
      }
      System.out.println();
    }

    printMultiArray(createMultiArray(10, 3));

//    Scanner scanner = new Scanner(System.in);
//    String next = scanner.next();
//    System.out.println(next);
    System.out.println("-----------------------------");

    String hello = "Hello";
    String hello2 = new String("Hello");
    String hello3 = String.valueOf("Hello");


    System.out.println(System.getProperty("file.encoding"));
    System.out.println(Charset.defaultCharset());

    //-Dfile.encoding=UTF-16
    //new String(byte[] bytes, Charset charset)

    byte[] bytes = {1,2,3,4,5};
    byte[] bytes1 = hello.getBytes();
    String s = new String(hello.getBytes(), Charset.availableCharsets().get("windows-1251"));
    System.out.println(s);

    System.out.println("GoodBye".length());
    System.out.println("Hel" + "lo" + (2 + 2));

    for(char r: hello.toCharArray()) {
      System.out.println(r);
    }


    String newHello = "Hello";
    String newHello2 = "Hel" + "lo";
    final String hel = "Hel";
    newHello = "Hallo";
    String newHello4 = hel + "lo";

    System.out.println(newHello == newHello2);
    System.out.println(newHello.equals(newHello2));

    String newHello3 = new String("Hello");

    System.out.println(newHello == newHello3);
    System.out.println(newHello.equals(newHello3));


    System.out.println("----------------------");
    System.out.println(newHello == newHello4);
    System.out.println(newHello.equals(newHello4));

    System.out.println("----------------------");
    System.out.println(hello.codePointAt(1));
    System.out.println("hello  world   ".trim());

    // StringBuffer stringBuffer = new StringBuffer();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("qweqweqwe");
    stringBuilder.append("qweqweqwe");
    stringBuilder.append("qweqweqwe");
    stringBuilder.append("qweqweqwe");
    stringBuilder.append("qweqweqwe");
    String s1 = stringBuilder.toString();
    System.out.println(s1);

    String q = null;// ""
    String qq = "";
    System.out.println(qq.length());
    if (qq!=null && qq.equals("qq")) {

    }

    String[] strings = {"qqq", "qweq", "qwerqwe"};
    System.out.println(Arrays.toString(args));

    Scanner scanner = new Scanner(System.in);
    String i = scanner.nextLine();
  }

  public static int[][] createMultiArray(int length, int width) {
    int[][] multiArray = new int[length][width];
    for (int i = 0; i < multiArray.length; i++) {
      for (int j = 0; j < multiArray[0].length; j++) {
        multiArray[i][j] = i + j;
      }
    }
    return multiArray;
  }

  public static void printMultiArray(int[][] multiArray) {
    for (int i = 0; i < multiArray.length; i++) {
      for (int j = 0; j < multiArray[0].length; j++) {
        System.out.print(multiArray[i][j] + " ");
      }
      System.out.println();
    }
  }


  //public static void/Type name(Args)
  public static void print(int i, int j, boolean... q) {

  }

  //1. Создаешь сканер и из командной строки получаешь 2 String (состоят из цифр)
  //2. Преобразовываем в массив byte[]
  //3. Реализовать 2 метода: 1. сложение(массив, массив) = массив 2. вычитание




}
