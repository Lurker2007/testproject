package ru.home.inheritance;

public class Main {

  public static void main(String[] args) {
//    Box box = new Box(1, 2, 3);
//    WeightBox weightBox = new WeightBox(1, 2, 3, 4);
    Box newBox = new WeightBox(1, 2, 3, 4);
//    newBox = new ColoredBox(1, 2, 3, "Black");
//    int volume = newBox.volume();
    if (newBox instanceof WeightBox) {
      WeightBox newBox1 = (WeightBox) newBox;
      System.out.println(newBox1.getWeight());
    }
//    ColoredBox coloredBox = new ColoredBox(1,2,3, "White")

    Box box1 = new Box(1, 2, 3);
    Box box2 = new Box(1, 2, 4);
    System.out.println(box1.equals(box2));
  }


}
