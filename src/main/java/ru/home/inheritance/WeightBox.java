package ru.home.inheritance;

public class WeightBox extends Box {
  private int weight;

  public WeightBox(int width, int height, int lenght, int weight) {
    super(width, height, lenght);
    System.out.println("Конструктор WeightBox");
    this.weight = weight;
  }

  @Override
  public int volume() {
    return super.volume() * weight;
  }

  public int getWeight() {
    return weight;
  }
}
