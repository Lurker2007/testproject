package ru.home.inheritance;

public class Box {
  protected int width;
  protected int height;
  protected int lenght;

  public Box(int width, int height, int lenght) {
    System.out.println("Коснтруктор Box(int width, int height, int lenght)");
    this.width = width;
    this.height = height;
    this.lenght = lenght;
  }

  public int volume() {
    return width * height * lenght;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof Box) {
      Box box = (Box)obj;
      return (this.height == box.height
          && this.lenght == box.lenght
          && this.width == box.width);
    } else {
      return false;
    }
  }

  protected void bark() {
    System.out.println("Гав");
  }
}
