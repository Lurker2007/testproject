package ru.home.inheritance;

public class ColoredBox extends Box {
  private String color;

  public ColoredBox(int width, int height, int lenght, String color) {
    super(width, height, lenght);
    this.color = color;
  }

  public String getColor() {
    return color;
  }

  @Override
  public void bark() {
    super.bark();
  }
}
