package ru.home.inheritance.animal;

public class Cat extends Animal {
  @Override
  public void makeNoise() {
    System.out.println("Мяу");
  }

  @Override
  public String getName() {
    return "name";
  }
}
