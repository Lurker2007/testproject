package ru.home.inheritance.animal;

public class Cow extends Animal {
  @Override
  public void makeNoise() {
    System.out.println("Мууууу");
  }
}
