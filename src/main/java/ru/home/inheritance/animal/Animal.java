package ru.home.inheritance.animal;

public abstract class Animal {

  public abstract void makeNoise();

  public String getName() {
    return "name";
  }
}
