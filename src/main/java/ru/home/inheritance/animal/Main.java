package ru.home.inheritance.animal;

public class Main {
  public static void main(String[] args) {
    Animal haski = new Haski();
    //haski.makeNoise();
    Animal cat  = new Cat();

    if (haski instanceof Dog) {
      Dog haski1 = (Dog) haski;
      haski1.makeNoise();
      haski1.getDogName();
    }
    if(!(cat instanceof Dog)) {
      System.out.println("Кошка не собака");
    }


    Animal[] animals = new Animal[10];
    animals[0] = new Cow();
    animals[1] = new Cow();
    animals[2] = new Dog();
    animals[3] = new Cat();
    animals[4] = new Dog();
    animals[5] = new Dog();
    animals[6] = new Dog();
    animals[7] = new Cat();
    animals[8] = new Cat();
    animals[9] = new Cow();

    for (Animal animal: animals) {
      animal.makeNoise();
    }

    Cat cat1 = new Cat();
    Cat cat2 = cat1;
    // 1. equals
    // a.equals(a) = true
    // a.equals(b) => b.equals(a)
    // a.equals(b) и b.equals(c) => a.equals(c)
    // a.equals(b) - true/false - CONST
    // a.equals(null) - false
    // 2. hashCode
    // a.equals(b) = true => a.hash = b.hash, но если a.hash = b.hash !=> a.equals(b) = true
    // 3. toString() - строкое представление объекта
    // 4. finalize() - метод, который вызывается перед удалением объекта сборщиком мусора
    // 5. getClass() - метаинформация по классу (какие классы, какие методы)
    // 6. clone() - клонирование, глубокое (мы сами определяем глубину) и поверхностное (толкьо ссылки на вложенные объекты)
    // 7. wait()
    // 8. notify()
    // 9. notifyAll()
    System.out.println(cat1.equals(cat2));



  }
}
