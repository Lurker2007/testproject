package ru.home.nestedclass;

public class Boeing747 {
  private int manufactureYear;
  private static int maxPass = 300;

  Boeing747() {
    Drawing.getPassCount();
  }

  //Nested
  public static class Drawing {


    public static int getPassCount() {
      return maxPass;
    }
  }
}
