package ru.home.innerclass;

public class Bicycle {
  private String model;
  private Seat seat;
  private HandleBar handleBar;

  public Bicycle(String model) {
    this.model = model;
    seat = new Seat();
    handleBar = new HandleBar();
  }

  public void run() {
    prepareRun();
    System.out.println("Кручу педали");
    handleBar.right();
  }

  private void prepareRun() {
    seat.setSeat();
  }
  //Inner class
  public class Seat {
//    static String getSomeThing() {
//      return null;
//    }
    private String type;

    private void setSeat() {
      System.out.println(handleBar + " Регулирую сиденье");
    }
  }

  public class HandleBar {
    public void right() {
      System.out.println("Поворачиваю направо");
    }
  }

}



